const { check, validationResult } = require('express-validator');
const config = require('../../config/constants.json');


exports.validatePatientCreation = [

    check('patientId').not().isEmpty().withMessage('patientId can not be empty!'),

    check('patientName').not().isEmpty().withMessage('patientName can not be empty!'),

    check('devicePlatform').not().isEmpty().withMessage('devicePlatform can not be empty!'),

    check('devicePlatform').isIn([config.CGM_TYPE.DEXCOM,config.CGM_TYPE.LIBRE]).withMessage(`devicePlatform can only have values as ${config.CGM_TYPE.DEXCOM} or ${config.CGM_TYPE.LIBRE}`),
    
    (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(400).send({
        status:false,
        message:errors.array()
      })
    next();
  },
];

exports.validatePatientUpdate = [

    check('patientId').not().isEmpty().withMessage('patientId can not be empty!'),

    check('patientName').not().isEmpty().withMessage('patientName can not be empty!'),

    check('devicePlatform').not().isEmpty().withMessage('devicePlatform can not be empty!'),

    check('status').not().isEmpty().withMessage('status can not be empty!'),

    check('status').isIn([config.STATUS.ACTIVE,config.STATUS.INACTIVE]).withMessage(`status can only have values as ${config.STATUS.ACTIVE} or ${config.STATUS.INACTIVE}`),

    check('devicePlatform').isIn([config.CGM_TYPE.DEXCOM,config.CGM_TYPE.LIBRE]).withMessage(`devicePlatform can only have values as ${config.CGM_TYPE.DEXCOM} or ${config.CGM_TYPE.LIBRE}`),
    
    (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(400).send({
        status:false,
        message:errors.array()
      })
    next();
  },
];

exports.validateDexcomConnection = [

    check('patientId').not().isEmpty().withMessage('patientId can not be empty!'),

    check('authCode').not().isEmpty().withMessage('authCode can not be empty!'),
    
    (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(400).send({
        status:false,
        message:errors.array()
      })
    next();
  },
];