
const cgmModel = require('../models/cgm-readings.model');

exports.getCGMReadingForPatient = async(patientId) => {
    try {

        return await cgmModel.findOne({patientId:patientId});

    }catch(error){
        throw error
    }
}