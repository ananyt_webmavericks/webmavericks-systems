
const config = require('../../config/constants.json');
const patientModel = require('../models/patients.model');
const moment = require('moment');

exports.createPatient = async (requestData) => {
    try {

        const newPatient = new patientModel();
        newPatient.patientId = requestData.patientId
        newPatient.patientName = requestData.patientName
        newPatient.devicePlatform = requestData.devicePlatform

        return await newPatient.save();
        

    }catch(error){
        throw error
    }
}

exports.updatePatient = async (requestData) => {

    try {

        const getPatientById = await patientModel.findOne({patientId:requestData.patientId})

        if(!getPatientById) {
            throw new Error('Patient not found!')
        }

        if(requestData.status) {
            getPatientById.status = requestData.status
        }

        if(requestData.patientName) {
            getPatientById.patientName = requestData.patientName
        }

        if(requestData.devicePlatform) {
            getPatientById.devicePlatform = requestData.devicePlatform
        }

        return await getPatientById.save();

    }catch(error){
        throw error
    }
}

exports.updateAccessInfoPatient = async (patientId,getAccessInfo,authCode) => {
    try {

        const getPatientById = await patientModel.findOne({patientId:patientId});

        if(!getPatientById) {
            throw new Error("Patient not found!")
        }

        if(getPatientById.devicePlatform !== config.CGM_TYPE.DEXCOM){
            throw new Error('Trying to connect wrong device platform for Patient!')
        }

        getPatientById.metaData = {}
        getPatientById.metaData.accessToken = getAccessInfo.access_token
        getPatientById.metaData.refreshToken = getAccessInfo.refresh_token
        getPatientById.metaData.expiresIn = getAccessInfo.expires_in
        getPatientById.metaData.tokenType = getAccessInfo.token_type
        getPatientById.metaData.generatedAt = moment().unix();
        getPatientById.metaData.authCode = authCode

        return await getPatientById.save();

    }catch(error){
        throw error
    }
}

exports.getPatientInfoById = async (patientId) => {
    try {

        return await patientModel.findOne({patientId:patientId},{patientId:1,patientName:1,devicePlatform:1});

    }catch(error){
        throw error
    }
}