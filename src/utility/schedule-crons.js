const CronJob = require('cron').CronJob

module.exports = (...crons) => {
  crons.forEach(c => {
    new CronJob(c[0], async () => {
      await c[1]()
    }).start()
  })
}
