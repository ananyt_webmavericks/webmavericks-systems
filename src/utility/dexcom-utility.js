
const axios = require('axios');
const qs = require('qs');

exports.authenticatePatientWithDexcom = async (authCode) => {
    try {

        const data = qs.stringify({
            "client_id":process.env.DEXCOM_CLIENT_ID,
            "client_secret":process.env.DEXCOM_CLIENT_SECRET,
            "code":authCode,
            "grant_type":process.env.DEXCOM_GRANT_TYPE,
            "redirect_uri":process.env.DEXCOM_REDIRECT_URI
        });

        const config = {

            method: 'post',
            url: `${process.env.DEXCOM_URL}/oauth2/token`,
            headers: { 
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            data : data
        };
        
        const authenticate = await axios(config);

        return authenticate.data

    }catch(error){
        throw error
    }
}

exports.getGlucoseMeasurements = async (accessToken) => {
    try {

        const config = {
            method: 'get',
            url: `${process.env.DEXCOM_URL}/users/self/egvs?startDate=2019-12-24T04:00:07&endDate=2020-02-14T14:46:39`,
            headers: { 
                'Authorization': `Bearer ${accessToken}`
            }
        };

        const getValues = await axios(config);

        return getValues.data


    }catch(error){
        throw error
    }
}

exports.getInsulinMeasurements = async (accessToken) => {
    try {

        const config = {
            method: 'get',
            url: `${process.env.DEXCOM_URL}/users/self/events?startDate=2019-12-24T04:00:07&endDate=2020-02-14T14:46:39`,
            headers: { 
                'Authorization': `Bearer ${accessToken}`
            }
        };

        const getValues = await axios(config);

        return getValues.data


    }catch(error){
        throw error
    }
}

exports.getDeviceInformation = async (accessToken) => {
    try {

        const config = {
            method: 'get',
            url: `${process.env.DEXCOM_URL}/users/self/devices?startDate=2019-12-24T04:00:07&endDate=2020-02-14T14:46:39`,
            headers: { 
                'Authorization': `Bearer ${accessToken}`
            }
        };

        const getValues = await axios(config);

        return getValues.data


    }catch(error){
        throw error
    }
}