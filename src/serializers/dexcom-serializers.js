
const moment = require('moment');

exports.serializeDexcomGlucoseValue = (data,deviceInfo) => {
    try {

        return {
            timestamps:moment().unix(),
            utcOffset:0,
            guid:deviceInfo.transmitterGeneration,
            syncTimestamp:moment().unix(),
            glucoseValue:data.value,
            trendArrow:data.trend,
            units:"mg/dL",
            mealTag:null,
            mealTagSource:null,
            deviceId:deviceInfo.displayApp,
            deviceType:deviceInfo.transmitterGeneration,
            systemTime:data.systemTime,
            displayTime:data.displayTime
        }

    }catch(error){
        throw error
    }
}

exports.serializeDexcomInsulinValue = (data,deviceInfo) => {
    try {

        return {
            timestamp:moment().unix(),
            utcOffset:0,
            guid: deviceInfo.transmitterGeneration,
            updatedAt:moment().unix(),
            syncTimestamp:moment().unix(),
            insulinValue: data.value,
            deviceType:deviceInfo.transmitterGeneration ,
            deviceId:deviceInfo.displayApp ,
            mealTag: null,
            mealTagSource:null ,
            units: "units"
        }

    }catch(error){
        throw error
    }
}