const express = require("express");
const router = express.Router();

const patientMiddleware = require('../middlewares/patient.middleware');
const patientController = require('../controllers/patient.controller');

router.post("/create-patient",patientMiddleware.validatePatientCreation,patientController.createPatient)
router.patch("/update-patient",patientMiddleware.validatePatientUpdate,patientController.updatePatient)
router.patch("/connect-dexcom-device",patientMiddleware.validateDexcomConnection,patientController.createDexcomConnection)
router.get("/get-patient-cgm-data-by-id/:patientId",patientController.getPatientCGMDataById)

module.exports = router;
