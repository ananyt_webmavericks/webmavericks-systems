
const patientHelpers = require('../helpers/patients.helpers');
const dexcomUtility = require('../utility/dexcom-utility');
const cgmHelper = require('../helpers/cgm-helpers');

exports.createPatient = async (req,res,next) => {

    try {

        const requestData = req.body

        const createPatient = await patientHelpers.createPatient(requestData);

        res.status(200).send({
            status:true,
            data:createPatient,
            message:"Patient created Successfully!"
        })


    }catch(error) {
        res.status(400).send({
            status:false,
            message:error.message
        })
    }
}

exports.updatePatient = async (req,res,next) => {
    try {

        const requestData = req.body

        const updatePatient = await patientHelpers.updatePatient(requestData);

        res.status(200).send({
            status:true,
            data:updatePatient,
            message:"Patient updated Successfully!"
        })

    }catch(error) {
        res.status(400).send({
            status:false,
            message:error.message
        })
    }
}

exports.createDexcomConnection = async (req,res,next) => {
    try {

        const { patientId , authCode } = req.body

        const getAccessInfo = await dexcomUtility.authenticatePatientWithDexcom(authCode);

        const storeAccessInfo = await patientHelpers.updateAccessInfoPatient(patientId,getAccessInfo,authCode);

        res.status(200).send({
            status:true,
            data:storeAccessInfo,
            message:"Dexcom connected successfully!"
        })


    }catch(error) {
        res.status(400).send({
            status:false,
            message:error.message
        })
    }
}

exports.getPatientCGMDataById = async (req,res,next) => {
    try {

        const patientId = req.params['patientId'];

        const getPatientInfo = await patientHelpers.getPatientInfoById(patientId);

        const getPatientCGMReading = await cgmHelper.getCGMReadingForPatient(patientId);

        res.status(200).send({
            status:true,
            data:{
                patientInfo:getPatientInfo,
                patientReading:getPatientCGMReading
            },
            message:"Information fetched successfully!"
        })
        
    }catch(error){
        res.status(400).send({
            status:false,
            message:error.message
        })
    }
}