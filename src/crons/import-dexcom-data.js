
const patientModel = require('../models/patients.model');
const config = require('../../config/constants.json');
const moment = require('moment');
const dexcomUtility = require('../utility/dexcom-utility');
const dexcomSerializer = require('../serializers/dexcom-serializers');
const cgmModel = require('../models/cgm-readings.model');

module.exports = async () => {
    try {

        const getAllPatients = await patientModel.find({status:config.STATUS.ACTIVE,devicePlatform:config.CGM_TYPE.DEXCOM});

        for(let i = 0 ; i < getAllPatients.length; i++) {
            const patient = getAllPatients[i]
            const timeNow = moment().unix();
            const expiryTime = Number(patient.metaData.generatedAt) + patient.metaData.expiresIn

            if(timeNow > expiryTime) {
                const newToken = await dexcomUtility.authenticatePatientWithDexcom(patient.metaData.authCode)
                const updatePatient = await patientModel.findOneAndUpdate({patientId:patient.patientId},{
                    $set:{
                        "metaData.accessToken":newToken.access_token,
                        "metaData.refreshToken":newToken.refresh_token,
                        "metaData.expiresIn":newToken.expires_in,
                        "metaData.tokenType":newToken.token_type,
                        "metaData.generatedAt":moment().unix(),
                        "metaData.authCode":patient.metaData.authCode,
                    }
                })
                patient = updatePatient
            }

            const callGlucoseAPI = dexcomUtility.getGlucoseMeasurements(patient.metaData.accessToken);
            const callInsulinAPI = dexcomUtility.getInsulinMeasurements(patient.metaData.accessToken);
            const getDeviceInfoAPI = dexcomUtility.getDeviceInformation(patient.metaData.accessToken);

            const getValues = await Promise.all([callGlucoseAPI,callInsulinAPI,getDeviceInfoAPI]);

            const glucoseData = getValues[0];
            const insulinData = await filterInsulinData(getValues[1]);
            const getDeviceInfo = getValues[2];
            const lengthOfDevices = getDeviceInfo.devices.length
            const latestDeviceInfo = getDeviceInfo.devices[lengthOfDevices-1];

            const getPatientCGMReadings = await cgmModel.findOne({patientId:patient.patientId});
            let patientGlucoseReading = [];
            let patientInsulinReading = [];

            if(getPatientCGMReadings) {
                patientGlucoseReading = getPatientCGMReadings.cgmReadings
                patientInsulinReading = getPatientCGMReadings.pumpsReadings
            }

            for(let j = 0 ; j < glucoseData.egvs.length ; j++) {
                const serializeDexcomData = await dexcomSerializer.serializeDexcomGlucoseValue(glucoseData.egvs[j],latestDeviceInfo);
                patientGlucoseReading.push(serializeDexcomData)
            }

            for(let k = 0 ; k < insulinData.length ; k++){
                const serializeDexcomData = await dexcomSerializer.serializeDexcomInsulinValue(insulinData[k],latestDeviceInfo);
                patientInsulinReading.push(serializeDexcomData)
            }

            await cgmModel.findOneAndUpdate({patientId:patient.patientId},{$set:{
                cgmReadings:patientGlucoseReading,
                patientId:patient.patientId,
                pumpsReadings:patientInsulinReading
            }},{upsert:true});
        }

    }catch(error){
        console.log(error)
    }
}

function filterInsulinData(eventsData) {
    let insulinData = []
    for(let i = 0 ; i < eventsData.events.length ; i++ ) {
        if(eventsData.events[i].eventType === 'insulin') {
            const object = {
                systemtTime:eventsData.events[i].systemTime,
                displayTime:eventsData.events[i].displayTime,
                value:eventsData.events[i].value,
                unit:eventsData.events[i].unit
            }
            insulinData.push(object)
        }
    }

    return insulinData
}