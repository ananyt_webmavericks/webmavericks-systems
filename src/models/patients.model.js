const mongoose = require('mongoose');
const config = require('../../config/constants.json');

const patientSchema = mongoose.Schema({

    patientId : {type:String, required:true},
    patientName:{type:String, required:true},
    devicePlatform:{type:String, required:true , enum:[config.CGM_TYPE.DEXCOM,config.CGM_TYPE.LIBRE]},
    metaData:{
        accessToken:{type:String},
        refreshToken:{type:String},
        expiresIn:{type:Number},
        tokenType:{type:String},
        generatedAt:{type:String},
        authCode:{type:String}
    },
    filesUploaded:[
        {
            fileName:{type:String},
            uploadedAt:{type:String}
        }
    ],
    status:{type:String, default:config.STATUS.ACTIVE , enum:[config.STATUS.ACTIVE,config.STATUS.INACTIVE]}

},{timestamps:true});

module.exports = mongoose.model('patients', patientSchema);