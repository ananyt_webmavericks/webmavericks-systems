
const mongoose = require("mongoose");

const cgmReadings = mongoose.Schema({

    patientId: {type:String,required:true},
    cgmReadings:[
        {
            timestamps:{type:String},
            utcOffset:{type:String},
            guid:{type:String},
            syncTimestamp:{type:String},
            glucoseValue:{type:String},
            trendArrow:{type:String},
            units:{type:String},
            mealTag:{type:String},
            mealTagSource:{type:String},
            deviceId:{type:String},
            deviceType:{type:String},
            systemTime:{type:String},
            displayTime:{type:String}
        }
    ],
    pumpsReadings:[
        {
            timestamp: {type:String},
            utcOffset: {type:String},
            guid: {type:String},
            updatedAt: {type:String},
            syncTimestamp:{type:String},
            insulinValue: {type:String},
            deviceType: {type:String},
            deviceId: {type:String},
            mealTag: {type:String},
            mealTagSource: {type:String},
            units: {type:String}
        }
    ]

},{timestamps:true});

module.exports = mongoose.model("cgm-readings",cgmReadings)