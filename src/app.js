const express = require("express");
const app = express();
const cors = require("cors");
// const multer  = require('multer');
const bodyParser = require("body-parser");
const mongoose = require('mongoose');
const scheduleCrons = require('./utility/schedule-crons');

//importing app routes files
const patientRoutes = require("./routes/patients.routes");

//importing environment file
require("custom-env").env(process.env.NODE_ENV, "../env");
const port = process.env.APP_PORT || 8042;
app.use(cors());
// app.use(multer);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/***************Mongodb configuratrion********************/
const DATABASE_URL = process.env.MONGO_URL
//configuration ===============================================================
mongoose.set('strictQuery', false);
mongoose.connect(DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology : true
}).then(() => {console.log("DATABASE CONNECTED!!")})
.catch((err)=>{
    console.log("Error in Connecting Database");
    console.log("Error Occurred:",err)
    process.exit(0);
}); // connect to our database

// scheduleCrons(
//     ['* * * * * *',require('./crons/import-dexcom-data')]
// )

// mounting routes
app.use("/api/v1/patient", patientRoutes);

// launching app
app.listen(port);
console.log("Server is listening at " + port);

exports = module.exports = app;
